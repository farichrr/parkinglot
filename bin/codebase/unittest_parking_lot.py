import unittest
from main import ParkingLot
class TestParkingLot(unittest.TestCase):

	def test_create_parking_lot(self):
		parkingLot = ParkingLot()
		tes = parkingLot.createParkingLot(6)
		self.assertEqual(6,tes)

	def test_park(self):
		parkingLot = ParkingLot()
		tes = parkingLot.createParkingLot(6)
		tes = parkingLot.park("KA-01-HH-1234")
		tes = parkingLot.park("KA-01-HH-9999")
		tes = parkingLot.park("KA-01-BB-0001")
		tes = parkingLot.park("KA-01-HH-7777")
		tes = parkingLot.park("KA-01-HH-2701")
		tes = parkingLot.park("KA-01-HH-3141")
		self.assertNotEqual(-1,tes)


	def test_leave(self):
		parkingLot = ParkingLot()
		tes = parkingLot.createParkingLot(6)
		tes = parkingLot.park("KA-01-HH-1234")
		tes = parkingLot.leave(1)
		self.assertEqual(True,tes)

	def test_status(self):
		parkingLot = ParkingLot()
		parkingLot.createParkingLot(6)
		parkingLot.park("KA-09-HH-1234")
		parkingLot.park("KA-01-HH-9999")
		parkingLot.park("CA-09-IO-1111")
		parkingLot.park("KA-01-HH-7777")
		parkingLot.park("KA-01-HH-2701")
		parkingLot.park("KA-01-P-333")
		tes = parkingLot.status()
		self.assertEqual(None, tes)


if __name__ == '__main__':
	unittest.main()