import Vehicle
import argparse
import sys

if sys.version_info[0] == 2:
    input = input()


class ParkingLot:
    def __init__(self):
        self.capacity = 0
        self.slotid = 0
        self.numOfOccupiedSlots = 0

    def createParkingLot(self, capacity):
        self.slots = [-1] * capacity
        self.capacity = capacity
        return self.capacity

    def getEmptySlot(self):
        for i in range(len(self.slots)):
            if self.slots[i] == -1:
                return i

    def park(self, regno):
        if self.numOfOccupiedSlots < self.capacity:
            slotid = self.getEmptySlot()
            self.slots[slotid] = Vehicle.Car(regno)
            self.slotid = self.slotid + 1
            self.numOfOccupiedSlots = self.numOfOccupiedSlots + 1
            return slotid + 1
        else:
            return -1

    def leave(self, slotid):

        if self.numOfOccupiedSlots > 0 and self.slots[slotid - 1] != -1:
            self.slots[slotid - 1] = -1
            self.numOfOccupiedSlots = self.numOfOccupiedSlots - 1
            return True
        else:
            return False

    def status(self):
        print("Slot No.\tRegistration No.")
        for i in range(len(self.slots)):
            if self.slots[i] != -1:
                print(str(i + 1) + "\t\t\t" + str(self.slots[i].regno) + "\t")
            else:
                continue

    def show(self, line):
        if line.startswith('create_parking_lot'):
            n = int(line.split(' ')[1])
            res = self.createParkingLot(n)
            print('Created a parking lot with ' + str(res) + ' slots')

        elif line.startswith('park'):
            regno = line.split(' ')[1]
            res = self.park(regno)
            if res == -1:
                print("Sorry, parking lot is full")
            else:
                print('Allocated slot number: ' + str(res))

        elif line.startswith('leave'):
            regno = line.split(' ')[1]
            hour = line.split(' ')[2]
            slotno = self.getSlotNoFromRegNo(regno)
            charge = int(hour) - 2
            if slotno == -1:
                print("Registration number "+regno+" not found")
            else:
                if charge == 0:
                    total = 10
                    status = self.leave(slotno)
                    if status:
                        print('Registration Number ' + str(regno) + ' with' + str(slotno) + ' is free with charge ' + str(total))

                elif charge != 0:
                    charge_additional = charge * 10
                    total = 10+charge_additional
                    status = self.leave(slotno)
                    if status:
                        print('Registration Number ' + str(regno) + ' with Slot Number ' + str(slotno) + ' is free with charge ' + str(total))

        elif line.startswith('status'):
            self.status()

        elif line.startswith('exit'):
            exit(0)

    def getSlotNoFromRegNo(self, regno):
        for i in range(len(self.slots)):
            if self.slots[i].regno == regno:
                return i + 1
            else:
                continue
        return -1


def main():
    parkinglot = ParkingLot()
    parser = argparse.ArgumentParser()
    parser.add_argument(dest='src_file', help="Input File")
    args = parser.parse_args()

    if args.src_file:
        with open(args.src_file) as f:
            for line in f:
                line = line.rstrip('\n')
                parkinglot.show(line)
    else:
        while True:
            line = input("")
            parkinglot.show(line)


if __name__ == '__main__':
    main()