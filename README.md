# parking_lot

To clone the repository
```sh
git clone https://gitlab.com/farichrr/parkinglot.git
```

## Installation dependencies

Please install python or python3 on your machine

[Python](https://www.python.org/downloads/)

or you can use brew.sh

```sh
brew install python
```
- Basic command 

1. `create_parking_lot n` - Create a parking lot base n input
2. `park car_platenumber` - Parks a car with plate number
3. `status` - Prints current parking lot and parked car plate number.
4. `leave car_platenumber` - Removes car from slot with plate number input
## Run binary files program

```sh
$ ./bin/setup

-----basic command-----
create_parking_lot (n) *n integer
park (car) *car platenumber(B-1234-ABC)
leave (car) *car platenumber(B-1234-ABC)
status *to show current parking_lot

---------note---------
ctrl+C to exit or command+C
-----------------------
```
 ![p1](https://gitlab.com/farichrr/parkinglot/-/raw/main/Screenshot_20211103_142011.png)
 
## Run functional test

```sh
$ ./bin/run_functional_tests
```
![p1](https://gitlab.com/farichrr/parkinglot/-/raw/main/Screenshot_20211103_142228.png)

## Run test by text input

```sh
$ ./bin/parking_lot ./bin/file_input.txt
```
![p1](https://gitlab.com/farichrr/parkinglot/-/raw/main/Screenshot_20211103_145801.png)

## Unit test code
![p1](https://gitlab.com/farichrr/parkinglot/-/raw/main/Screenshot_20211103_142327.png)


